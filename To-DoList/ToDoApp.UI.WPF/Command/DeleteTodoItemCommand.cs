﻿using BusinessLayer;
using DataAccess.Repository;
using DataAccess;

namespace ToDoApp.UI.WPF.Command;
// Клас, що реалізує команду для видалення завдання.
public class DeleteTodoItemCommand : ICommand
{
    private readonly int _taskId;
    // Конструктор, який приймає ідентифікатор завдання для видалення.
    public DeleteTodoItemCommand(int taskId)
    {
        _taskId = taskId;
    }
    // Метод для виконання команди. Він видаляє завдання з бази даних.
    public async void Execute()
    {
        var todoService = new TodoService(new TodoRepository(new AppDbContext()));
        await todoService.RemoveTaskAsync(_taskId);
    }

    // Метод для скасування команди. У цьому випадку, він не реалізований, оскільки скасування видалення завдання не підтримується.
    public void Undo()
    {
        
    }
}
