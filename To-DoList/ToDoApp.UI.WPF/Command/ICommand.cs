﻿namespace ToDoApp.UI.WPF.Command;

public interface ICommand
{
    void Execute();
    void Undo();
}
