﻿using System;
using System.Windows.Data;
using System.Windows.Media;

namespace ToDoApp.UI.WPF.Converter;
// Клас для конвертації пріоритету завдання в колір.
public class PriorityToColorConverter : IValueConverter
{
    // Метод для конвертації пріоритету в колір
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
        string priority = (string)value;

        switch (priority)
        {
            case "Високий":
                return Brushes.Red;
            case "Середній":
                return Brushes.Yellow;
            case "Низький":
                return Brushes.Green;
            default:
                return Brushes.Black;
        }
    }

    // Метод для конвертації кольору назад в пріоритет.
    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
        throw new NotImplementedException(); // Цей метод не реалізований, оскільки конвертація кольору назад в пріоритет не потрібна.
    }
}
