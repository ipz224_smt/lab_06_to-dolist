﻿using Model;
using Model.State;
using System;
using System.Windows;
using System.Windows.Controls;
using ToDoApp.UI.WPF.Command;
using ToDoApp.UI.WPF.ViewModel;
using System.Threading.Tasks;
using BusinessLayer.Strategy;
using BusinessLayer;
using BusinessLayer.Observer;
using DataAccess;
using DataAccess.Repository;

namespace ToDoApp.UI.WPF.View;  

public partial class MainWindow : Window
{
    private readonly MainWindowViewModel _viewModel;
    private CommandInvoker _commandInvoker = new CommandInvoker();
    private readonly HistoryService _historyService;

    // Конструктор MainWindow, де встановлюється ViewModel, DataContext, ініціалізується HistoryService та підписується на подію завантаження вікна.
    public MainWindow()
    {
        _viewModel = new MainWindowViewModel();
        DataContext = _viewModel;
        _historyService = new HistoryService(new HistoryRepository(new AppDbContext()));
        InitializeComponent();

        if (_historyService is IObserver observer)
        {
            _historyService.Attach(observer);
        }
        calendar.SelectedDate = DateTime.Today;
        _viewModel.SelectedDate = DateTime.Today;
        Loaded += MainWindow_Loaded;
    }

    // Метод, який викликається при завантаженні вікна. Він завантажує історію та оновлює дані.
    private async void MainWindow_Loaded(object sender, RoutedEventArgs e)
    {
        await ShowHistory();
        await Update();
    }

    // Обробник події зміни вибраної дати в календарі. Він оновлює вибрану дату в ViewModel.
    private void Calendar_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
    {
        _viewModel.SelectedDate = (DateTime)calendar.SelectedDate;
    }

    // Обробник натискання кнопки "Додати задачу". Він створює нову задачу, виконує команду додавання, логує історію та оновлює дані.
    private async void AddTaskButton_Click(object sender, RoutedEventArgs e)
    {
        var title = TitleTextBox.Text;
        var description = DescriptionTextBox.Text;
        var priority = PriorityComboBox.SelectedItem as ComboBoxItem;
        var priorityText = priority?.Content.ToString();
        var deadline = calendar.SelectedDate ?? DateTime.Today;

        var todoItem = new TodoItem
        {
            Title = title,
            Description = description,
            Priority = priorityText,
            Deadline = deadline,
            UserId = _viewModel.UserId
        };

        var addCommand = new AddTodoItemCommand(todoItem);
        _commandInvoker.SetCommand(addCommand);
        _commandInvoker.ExecuteCommand();
        await _historyService.LogTaskHistoryAsync("Завдання Додано", todoItem);
        await ShowHistory();
        await Update();
    }

    // Обробник натискання кнопки "Видалити задачу". Він підтверджує видалення, виконує команду видалення, логує історію, оновлює дані та видаляє задачу з колекції.
    private async void DeleteTaskButton_Click(object sender, RoutedEventArgs e)
    {
        var button = sender as Button;
        var task = button.DataContext as TodoItem;

        var result = MessageBox.Show("Ви дійсно бажаєте видалити цю задачу?", "Підтвердження видалення", MessageBoxButton.YesNo, MessageBoxImage.Question);

        if (result == MessageBoxResult.Yes)
        {
            var deleteCommand = new DeleteTodoItemCommand(task.Id);
            _commandInvoker.SetCommand(deleteCommand);
            _commandInvoker.ExecuteCommand();
            await _historyService.LogTaskHistoryAsync("Завдання Видалено", task);
            await ShowHistory();
            RemoveTask(task.Id);

        }
    }

    // Обробник натискання кнопки "Редагувати задачу". Він оновлює дані задачі, виконує команду оновлення, логує історію та оновлює дані.
    private async void EditTaskButton_Click(object sender, RoutedEventArgs e)
    {
        var button = sender as Button;
        var task = button.DataContext as TodoItem;

        if (task != null)
        {
            var title = TitleTextBox.Text;
            var description = DescriptionTextBox.Text;
            var priority = PriorityComboBox.SelectedItem as ComboBoxItem;
            var priorityText = priority?.Content.ToString();

            if (string.IsNullOrWhiteSpace(title) || string.IsNullOrWhiteSpace(description) || string.IsNullOrWhiteSpace(priorityText))
            {
                MessageBox.Show("Заповніть всі поля перед оновленням таски.");
                return;
            }

            task.Title = title;
            task.Description = description;
            task.Priority = priorityText;

            var updateCommand = new UpdateTodoItemCommand(task);
            _commandInvoker.SetCommand(updateCommand);
            _commandInvoker.ExecuteCommand();
            ClearEditForm();
            await _historyService.LogTaskHistoryAsync("Завдання Оновлено", task);
            await ShowHistory();
            await Update();
        }
    }

    // Обробник перевірки чекбоксу (задача виконується). Він змінює стан задачі на виконаний, виконує команду оновлення.
    private void CheckBox_Checked(object sender, RoutedEventArgs e)
    {
        var checkBox = (CheckBox)sender;
        var task = (TodoItem)checkBox.DataContext;
        task.ChangeState(new CompletedState());
        var updateCommand = new UpdateTodoItemCommand(task);
        _commandInvoker.SetCommand(updateCommand);
        _commandInvoker.ExecuteCommand();
    }

    // Обробник відміни перевірки чекбоксу (задача не виконується). Він змінює стан задачі на активний, виконує команду оновлення.
    private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
    {
        var checkBox = (CheckBox)sender;
        var task = (TodoItem)checkBox.DataContext;
        task.ChangeState(new ActiveState());
        var updateCommand = new UpdateTodoItemCommand(task);
        _commandInvoker.SetCommand(updateCommand);
        _commandInvoker.ExecuteCommand();
    }

    // Обробник натискання кнопки "Відкласти задачу". Він змінює стан задачі на відкладений, виконує команду оновлення та оновлює дані.
    private async void PostponeTaskButton_Click(object sender, RoutedEventArgs e)
    {
        var button = (Button)sender;
        var task = button.DataContext as TodoItem;
        task.ChangeState(new PostponedState());
        var updateCommand = new UpdateTodoItemCommand(task);
        _commandInvoker.SetCommand(updateCommand);
        _commandInvoker.ExecuteCommand();
        await Update();
    }

    // Метод для очищення форми редагування задачі.
    private void ClearEditForm()
    {
        TitleTextBox.Text = string.Empty;
        DescriptionTextBox.Text = string.Empty;
        PriorityComboBox.SelectedIndex = 0;
    }

    // Асинхронний метод для оновлення даних у ViewModel.
    public async Task Update()
    {
        var viewModel = (MainWindowViewModel)DataContext;
        await viewModel.LoadTasksForDateAsync();
    }

    // Асинхронний метод для завантаження історії.
    public async Task ShowHistory()
    {
        var viewModel = (MainWindowViewModel)DataContext;
        await viewModel.LoadHistoryForCurrentUserAsync();
    }

    // Метод для видалення задачі з колекції ViewModel.
    private void RemoveTask(int taskId)
    {
        var viewModel = (MainWindowViewModel)DataContext;
        viewModel.RemoveTask(taskId);
    }

    // Обробник зміни вибору в комбобоксі фільтра пріоритету. Він встановлює відповідний стратегію фільтрації.
    private void PriorityFilterComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        var comboBox = sender as ComboBox;
        var selectedComboBoxItem = comboBox.SelectedItem as ComboBoxItem;
        var selectedPriority = selectedComboBoxItem?.Content.ToString();

        switch (selectedPriority)
        {
            case "Низький":
                _viewModel.TaskFilterStrategy = new PriorityFilterStrategy("Низький");
                break;
            case "Середній":
                _viewModel.TaskFilterStrategy = new PriorityFilterStrategy("Середній");
                break;
            case "Високий":
                _viewModel.TaskFilterStrategy = new PriorityFilterStrategy("Високий");
                break;
            default:
                break;
        }
    }

    // Обробник натискання кнопки "Очистити". Він підтверджує очищення історії, виконує очищення та завантажує оновлену історію.
    private async void ClearButton_Click(object sender, RoutedEventArgs e)
    {
        var result = MessageBox.Show("Ви дійсно бажаєте видалити всі записи з історії?", "Підтвердження видалення", MessageBoxButton.YesNo, MessageBoxImage.Question);

        if (result == MessageBoxResult.Yes)
        {
            await _historyService.DeleteAllHistoriesAsync();
            await ShowHistory();
        }
    }
}
