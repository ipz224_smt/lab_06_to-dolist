﻿using Microsoft.EntityFrameworkCore;
using Model;

namespace DataAccess;

public class AppDbContext : DbContext
{
    public DbSet<TodoItem> TodoItems { get; set; }
    public DbSet<User> Users { get; set; }
    public DbSet<History> Histories { get; set; }


    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlServer(Constants.DbConnectionString);
    }
        
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<TodoItem>()
            .HasOne(t => t.User)
            .WithMany(u => u.TodoItems)
            .HasForeignKey(t => t.UserId);

        modelBuilder.Entity<History>()
           .HasOne(t => t.User)
           .WithMany(u => u.Histories)
           .HasForeignKey(t => t.UserId);
        base.OnModelCreating(modelBuilder);
    }
}
