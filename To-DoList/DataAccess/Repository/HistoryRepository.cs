﻿using Microsoft.EntityFrameworkCore;
using Model;

namespace DataAccess.Repository;
// Клас, що реалізує репозиторій для роботи з історією завдань.
public class HistoryRepository : IHistoryRepository
{
    // Контекст бази даних для взаємодії з таблицею історії.
    private readonly AppDbContext _context;

    // Конструктор, який приймає контекст бази даних.
    public HistoryRepository(AppDbContext context)
    {
        _context = context;
    }

    // Асинхронний метод для додавання нового запису в історію.
    public async Task AddHistoryAsync(History history)
    {
        _context.Histories.Add(history);
        await _context.SaveChangesAsync();
    }

    // Асинхронний метод для видалення всіх записів історії.
    public async Task DeleteAllHistoriesAsync()
    {
        _context.Histories.RemoveRange(_context.Histories);
        await _context.SaveChangesAsync();
    }

    // Асинхронний метод для отримання всіх записів історії.
    public async Task<IEnumerable<History>> GetAllHistoriesAsync()
    {
        return await _context.Histories.Include(t => t.User).ToListAsync();
    }
}
