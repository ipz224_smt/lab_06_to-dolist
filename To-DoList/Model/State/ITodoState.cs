﻿namespace Model.State;

public interface ITodoState
{
    void Handle(TodoItem todoItem);
}
