﻿namespace Model.State;
// Клас, що реалізує стан "Виконано" для завдання.
public class CompletedState : ITodoState
{
    // Метод для обробки завдання в стані "Виконано".
    public void Handle(TodoItem todoItem)
    {
        todoItem.IsCompleted = true;
    }
}
