﻿namespace Model.State;
// Клас, що реалізує стан "Активний" для завдання.
public class ActiveState : ITodoState
{
    // Метод для обробки завдання в стані "Активний".
    public void Handle(TodoItem todoItem)
    {
        todoItem.IsCompleted = false;
    }
}
