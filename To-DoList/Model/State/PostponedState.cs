﻿namespace Model.State;
// Клас, що реалізує стан "Відкладено" для завдання.
public class PostponedState : ITodoState
{
    // Метод для обробки завдання в стані "Відкладено".
    public void Handle(TodoItem todoItem)
    {
        todoItem.IsCompleted = false;
        // Перевіряємо, чи є дедлайн для завдання.
        if (todoItem.Deadline.HasValue)
        {
            todoItem.Deadline = todoItem.Deadline.Value.Date.AddDays(1);
        }
        else
        {
            todoItem.Deadline = DateTime.Now.Date.AddDays(1);
        }
    }
}
