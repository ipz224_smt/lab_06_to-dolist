﻿using System.ComponentModel.DataAnnotations;

namespace Model;
public class History
{
    [Key]
    public int Id { get; set; }
    public string? Title { get; set; }
    public DateTime? Date { get; set; }
    public string? Description { get; set; }
    public int UserId { get; set; }
    public User? User { get; set; }
}
