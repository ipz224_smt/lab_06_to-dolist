﻿using Model;

namespace BusinessLayer;

public interface IUserService
{
    Task<bool> RegisterNewUserAsync(string email, string password);
    Task<User> GetUserByIdAsync(int id);
    Task AddUserAsync(User user);
}
