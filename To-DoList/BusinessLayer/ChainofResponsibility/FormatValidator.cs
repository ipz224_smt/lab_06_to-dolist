﻿using System.Text.RegularExpressions;

namespace BusinessLayer.ChainofResponsibility;
// Клас, що реалізує валідатор формату електронної пошти в ланцюгу відповідальності.
public class FormatValidator : ValidatorBase
{
    // Метод для перевірки валідності електронної пошти на основі формату.
    protected override bool IsValid(string email)
    {
        // Регулярний вираз для перевірки формату електронної пошти.
        var emailRegex = @"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$";
        bool isValid = Regex.IsMatch(email, emailRegex);

        if (!isValid && _nextValidator != null)
            return _nextValidator.IsValidEmail(email);

        return isValid;
    }
}
