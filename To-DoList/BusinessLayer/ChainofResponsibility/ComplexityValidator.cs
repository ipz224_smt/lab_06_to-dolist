﻿namespace BusinessLayer.ChainofResponsibility;
// Клас, що реалізує валідатор складності пароля в ланцюгу відповідальності.
public class ComplexityValidator : ValidatorBase
{
    private readonly int _minLength;
    private readonly int _minUpperCase;
    private readonly int _minLowerCase;
    // Конструктор, який приймає параметри для валідації складності пароля.
    public ComplexityValidator(int minLength = 6, int minUpperCase = 1, int minLowerCase = 1)
    {
        _minLength = minLength;
        _minUpperCase = minUpperCase;
        _minLowerCase = minLowerCase;
    }
    // Метод для перевірки валідності пароля на основі його складності.
    protected override bool IsValid(string password)
    {
        if (password.Length < _minLength)
            return false;

        if (password.Count(char.IsUpper) < _minUpperCase)
            return false;

        if (password.Count(char.IsLower) < _minLowerCase)
            return false;


        return true;
    }
}
