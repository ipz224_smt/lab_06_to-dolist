﻿namespace BusinessLayer.ChainofResponsibility;

public interface IValidator
{
    bool IsValidEmail(string email);
    bool IsValidPassword(string password);
}
