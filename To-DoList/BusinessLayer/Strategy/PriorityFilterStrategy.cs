﻿using Model;
namespace BusinessLayer.Strategy;
// Клас, що реалізує стратегію фільтрації завдань за пріоритетом.
public class PriorityFilterStrategy : ITaskFilterStrategy
{
    private readonly string _priority;

    // Конструктор, який приймає пріоритет для фільтрації.
    public PriorityFilterStrategy(string priority)
    {
        _priority = priority;
    }

    // Метод для фільтрації списку завдань за пріоритетом.
    public IEnumerable<TodoItem> Filter(IEnumerable<TodoItem> tasks)
    {
        return tasks.Where(t => t.Priority == _priority);
    }
}

