﻿using DataAccess.Repository;
using Model;

namespace BusinessLayer;
// Клас, що реалізує сервіс для роботи з користувачами.
public class UserService : IUserService
{
    private readonly IUserRepository _userRepository;

    // Конструктор, який приймає репозиторій для роботи з користувачами.
    public UserService(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    // Асинхронний метод для логіну користувача.
    public async Task<bool> LoginAsync(string email, string password)
    {
        var user = await _userRepository.GetUserByEmailAsync(email);

        if (user == null)
            return false;

        // Перевіряємо валідність пароля.
        var passwordVerified = PasswordHasher.Verify(password, user.PasswordHash);

        return passwordVerified;
    }

    // Асинхронний метод для реєстрації нового користувача.
    public async Task<bool> RegisterNewUserAsync(string email, string password)
    {
        var existingUser = await _userRepository.GetUserByEmailAsync(email);

        // Якщо користувач з такою електронною поштою вже існує, повертаємо false.
        if (existingUser != null)
            return false;

        // Хешуємо пароль.
        var hashedPassword = PasswordHasher.Hash(password);

        var user = new User
        {
            Email = email,
            PasswordHash = hashedPassword
        };

        await _userRepository.AddUserAsync(user);

        return true;
    }

    // Асинхронний метод для отримання користувача за ідентифікатором.
    public async Task<User> GetUserByIdAsync(int id)
    {
        return await _userRepository.GetUserByIdAsync(id);
    }

    // Асинхронний метод для отримання користувача за електронною поштою.
    public async Task<User> GetUserByEmailAsync(string email)
    {
        return await _userRepository.GetUserByEmailAsync(email);
    }

    // Асинхронний метод для додавання користувача.
    public async Task AddUserAsync(User user)
    {
        await _userRepository.AddUserAsync(user);
    }
}
