﻿using System.Security.Cryptography;

namespace BusinessLayer;
// Клас для хешування паролів та перевірки їх валідності.
public class PasswordHasher
{
    private const int SaltSize = 16;
    private const int KeySize = 32;
    private const int Iterations = 10000;

    // Метод для хешування пароля.
    public static string Hash(string password)
    {
        using (var algorithm = new Rfc2898DeriveBytes(
          password,
          SaltSize,
          Iterations,
          HashAlgorithmName.SHA256))
        {
            // Отримуємо ключ та сіль після хешування.
            var key = GetKey(algorithm);
            var salt = Convert.ToBase64String(algorithm.Salt);

            return $"{Iterations}.{salt}.{key}";
        }
    }

    // Метод для отримання ключа.
    private static string GetKey(Rfc2898DeriveBytes algorithm)
    {
        return Convert.ToBase64String(algorithm.GetBytes(KeySize));
    }

    // Метод для перевірки валідності пароля.
    public static bool Verify(string password, string hashedPassword)
    {
        // Розбиваємо хеш на частини.
        var parts = hashedPassword.Split('.', 3);

        // Перевіряємо формат хешу.
        if (parts.Length != 3)
        {
            throw new FormatException("Unexpected hash format. " +
              "Should be formatted as `{iterations}.{salt}.{hash}`");
        }

        // Отримуємо ітерації, сіль та ключ з хешу.
        var iterations = Convert.ToInt32(parts[0]);
        var salt = Convert.FromBase64String(parts[1]);
        var key = Convert.FromBase64String(parts[2]);

        using (var algorithm = new Rfc2898DeriveBytes(
          password,
          salt,
          iterations,
          HashAlgorithmName.SHA256))
        {
            // Отримуємо ключ для перевірки.
            var keyToCheck = algorithm.GetBytes(KeySize);

            // Порівнюємо ключ для перевірки з ключем з хешу.
            var verified = keyToCheck.SequenceEqual(key);

            return verified;
        }
    }
}
