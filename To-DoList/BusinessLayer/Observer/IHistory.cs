﻿using Model;

namespace BusinessLayer.Observer;
public interface IHistory
{
    void Attach(IObserver observer);
    void Detach(IObserver observer);
    void Notify(History history);
}
